package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.TestStr;

public class TestStrTest {
    @Test
    public void testGstr() throws Exception {
        final String resp = new TestStr("sandesh").gstr();
        assertEquals("resp", "sandesh", resp);
    }
}
